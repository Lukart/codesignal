package islandOfKnowledge;

public class AvoidObstacles {
    public static void main(String[] args) {
        int[] inputArray = {1, 4, 10, 6, 2}; //expected 7;
        System.out.println(avoidObstacles(inputArray));
    }

    static int avoidObstacles(int[] inputArray) {
        int jump = 1;
        boolean flag = true;
        while (flag) {
            jump++;
            flag = false;
            for (int i = 0; i < inputArray.length; i++)
                if (inputArray[i] % jump == 0) {
                    flag = true;
                    break;
                }
        }
        return jump;
    }
}