package islandOfKnowledge;

public class isIPv4Address {
    public static void main(String[] args) {
        String inputString = "172.166.254.1";
        System.out.println(isIPv4Address(inputString));
    }

    static boolean isIPv4Address(String inputString) {
        long countPoint = inputString.chars().filter(p -> p == '.').count();
        if ((inputString.charAt(0) == '.') || (countPoint != 3))
            return false;
        for (int i = 0; i <inputString.length() - 1; i++) {
            if(inputString.charAt(i) == '.' && inputString.charAt(i+1) =='.')
                return false;
        }
        int[] array = inputString.replaceAll("\\.", "").chars().toArray();
        for (int i = 0; i < array.length; i++) {
            if (array[i] > 57 || array[i] < 48)
                return false;
        }
        int point1 = inputString.indexOf(".");
        int point2 = inputString.indexOf(".", point1 + 1);
        int point3 = inputString.indexOf(".", point2 + 1);
        if(point1 > 4 || point2 > 8 || point3 > 12)
            return false;
        long ip1 = Integer.parseInt(inputString.substring(0, point1));
        long ip2 = Integer.parseInt(inputString.substring(point1 + 1, point2));
        long ip3 = Integer.parseInt(inputString.substring(point2 + 1, point3));
        long ip4 = Integer.parseInt(inputString.substring(point3 + 1));
        if ((ip1 >= 0 && ip1 <= 255) && (ip2 >= 0 && ip2 <= 255) && (ip3 >= 0 && ip3 <= 255) && (ip4 >= 0 && ip4 <= 255))
            return true;
        return false;
    }
}
