package islandOfKnowledge;

import java.util.Arrays;

public class ArrayMaximalAdjacentDifference {
    public static void main(String[] args) {
        int[] inputArray = {2, 4, 1, 0};
        System.out.println(arrayMaximalAdjacentDifference(inputArray));
    }

    static int arrayMaximalAdjacentDifference(int[] inputArray) {
        int[] check = new int[inputArray.length - 1];
        for (int i = 0; i <inputArray.length - 1; i++)
            check[i] = Math.abs(inputArray[i] - inputArray[i+1]);
        Arrays.sort(check);
        return check[check.length - 1];
    }
}
