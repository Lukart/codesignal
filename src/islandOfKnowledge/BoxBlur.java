package islandOfKnowledge;

import java.util.Arrays;

public class BoxBlur {

    public static void main(String[] args) {
        int[][] image =    {{7, 4, 0, 1, 5},
                            {5, 6, 2, 2, 4},
                            {6, 1, 7, 8, 2},
                            {1, 4, 2, 0, 1}};
        System.out.println(Arrays.deepToString(boxBlur(image)));
    }

    static int[][] boxBlur(int[][] image) {
        int[][] boxBlur = new int[image.length - 2][image[0].length - 2];
        for (int i = 0; i < image.length - 2; i++) {
            for (int j = 0; j < image[0].length - 2; j++) {
                boxBlur[i][j] = image[i][j] + image[i][j + 1] + image[i][j + 2] + image[i + 1][j] +
                                image[i + 1][j + 1] + image[i + 1][j + 2] + image[i+2][j] +
                                image[i + 2][j + 1] + image[i + 2][j + 2];
            }
        }
        return boxBlur;
    }
}
