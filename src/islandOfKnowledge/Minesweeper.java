package islandOfKnowledge;

import java.util.Arrays;

public class Minesweeper {
    public static void main(String[] args) {
        boolean[][] matrix = {{false, false, false},
                                {false, false, false}};
        System.out.println(Arrays.deepToString(minesweeper(matrix)));
    }

    static int[][] minesweeper(boolean[][] matrix) {
        int[][] temp = new int[matrix.length + 2][matrix[0].length + 2];
        boolean[][] temp2 = new boolean[matrix.length + 2][matrix[0].length + 2];
        int[][] minesweeper = new int[matrix.length][matrix[0].length];
        for (int i = 0; i < temp.length; i++) {
            Arrays.fill(temp[i], 100);
            Arrays.fill(temp2[i], false);
        }
        for (int i = 0; i < matrix.length; i++) {
            for (int j = 0; j < matrix[0].length; j++) {
                temp[i + 1][j + 1] = 0;
                temp2[i + 1][j + 1] = matrix[i][j];
            }
        }
        for (int i = 0; i < temp2.length; i++)
            for (int j = 0; j < temp2[0].length; j++)
                if (temp2[i][j]) {
                    (temp[i-1][j-1])++;
                    (temp[i-1][j])++;
                    (temp[i-1][j + 1])++;
                    (temp[i][j-1])++;
                    (temp[i][j + 1])++;
                    (temp[i + 1][j-1])++;
                    (temp[i + 1][j])++;
                    (temp[i + 1][j + 1])++;
                }
        for (int i = 0; i < minesweeper.length ; i++) {
            for (int j = 0; j < minesweeper[0].length ; j++) {
                minesweeper[i][j] = temp[i+1][j+1];
            }
        }
        return minesweeper;
    }
}

