package ownChallenge;

import java.util.Arrays;

public class MirrorDoubleArray {
    public static void main(String[] args) {
        int[] singleArray = {1};
        System.out.println(Arrays.toString(mirrorDoubleArray(singleArray)));
    }
    static int[] mirrorDoubleArray(int[] singleArray) {
        int size = singleArray.length;
        int newArraySize = 2 * size;
        int[] newArray = new int[newArraySize];
        for (int i = 0; i < size; i++) {
            newArray[i] = singleArray[i];
        }
        int secondIndex = size - 1;
        for (int i = size; i < newArraySize; i++) {
            newArray[i] = singleArray[secondIndex];
            secondIndex--;
        }
        return newArray;
    }
}
