package smoothSailing;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class SortByHeight {
    public static void main(String[] args) {

        int[] a = {-1, -1, 130, 180, -1, -1, 140, 160, -1, 150};
        System.out.println(Arrays.toString(sortByHeight(a)));

    }

    static int[] sortByHeight(int[] a) {
        List<Integer> indexOfTree = new ArrayList<>();
        List<Integer> result = new ArrayList<>();
        int[] b = new int[a.length];
        for (int i = 0; i < a.length; i++) {
            if (a[i] == -1) {
                indexOfTree.add(i);
            }
        }
        Arrays.sort(a);
        for (int i = 0; i < a.length; i++) {
            if (a[i] > 0) {
                result.add(a[i]);
            }
        }
        for (int i = 0; i <indexOfTree.size(); i++) {
            result.add(indexOfTree.get(i), -1);
        }
        for (int i = 0; i <result.size(); i++) {
            b[i] = result.get(i);
        }
        return b;
    }
}
