package smoothSailing;

import java.util.ArrayList;
import java.util.List;

public class CommonCharacterCount {
    public static void main(String[] args) {
        String s1 = "abbcd";
        String s2 = "abaaaaafghdd";
        System.out.println(commonCharacterCount(s1, s2));
    }

    static int commonCharacterCount(String s1, String s2) {
        int result = 0;
        String biggerOrEqual;
        String smaller;
        List<Character> string1 = new ArrayList<>();
        List<Character> string2 = new ArrayList<>();
        if(s1.length() >= s2.length()) {
            biggerOrEqual = s1;
            smaller = s2;
        }else {
            biggerOrEqual = s2;
            smaller = s1;
        }
        for (int i = 0; i < smaller.length() ; i++)
            string1.add(smaller.charAt(i));
        for (int i = 0; i <biggerOrEqual.length() ; i++)
            string2.add(biggerOrEqual.charAt(i));
        while (!string1.isEmpty()){
               if (string2.contains(string1.get(0))) {
                   Character character = string1.get(0);
                   result++;
                   string1.remove(0);
                   string2.remove(character);
               }else
                   string1.remove(0);
        }
        return result;
    }
}
