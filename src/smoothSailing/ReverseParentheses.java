package smoothSailing;
public class ReverseParentheses {

    public static void main(String[] args) {

        String inputString = "co(de(fight)s)";
        //expected result = "cosfighted"

        System.out.println(reverseParentheses(inputString));
    }
    static String reverseParentheses(String inputString) {
        String result;
        for (int i, j; (i = j = inputString.indexOf(")")) > 0; inputString = inputString.replace(inputString.substring(i, j + 1), result))
            for (result = "" ; inputString.charAt(--i) != '(';)
                result += inputString.charAt(i);
        return inputString;
    }
}
