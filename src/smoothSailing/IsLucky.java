package smoothSailing;

public class IsLucky {
    public static void main(String[] args) {

        System.out.println(isLucky(3360));
    }

    static boolean isLucky(int n) {
        int half = (String.valueOf(n).length())/2;
        int firstHalf = Integer.parseInt(String.valueOf(n).substring(0, half));
        int secondHalf = Integer.parseInt(String.valueOf(n).substring(half));
        int[]firstHalfArray = new int[(String.valueOf(firstHalf)).length()];
        int[]secondHalfArray = new int[(String.valueOf(secondHalf)).length()];
        int sumFirstHalf = 0;
        int sumSecondHalf = 0;
        for (int i = 0; i < firstHalfArray.length; i++) {
            firstHalfArray[i] = Integer.parseInt(String.valueOf(firstHalf).substring(i, i + 1));
            sumFirstHalf += firstHalfArray[i];
        }
        for (int i = 0; i < secondHalfArray.length; i++) {
            secondHalfArray[i] = Integer.parseInt(String.valueOf(secondHalf).substring(i, i+1));
            sumSecondHalf += secondHalfArray[i];
        }
        if(sumFirstHalf == sumSecondHalf) {
            return true;
        }else
            return false;
    }
}