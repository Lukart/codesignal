package smoothSailing;

import java.util.Arrays;

public class AllLongestString {
    public static void main(String[] args) {
        String[] inputArray = {"aa"};

        System.out.println(Arrays.toString(allLongestStrings(inputArray)));
    }

    static String[] allLongestStrings(String[] inputArray) {
        String temp;
        int count = 0;
        int maxStringLength;
        int howManyStringsAreMaximum = 0;

        if (inputArray.length == 1) {
            return inputArray;
        }else {
            while (count < inputArray.length) {
                for (int i = 0; i < inputArray.length - 1; i++) {
                    if (inputArray[i].length() < inputArray[i + 1].length()) {
                        temp = inputArray[i];
                        inputArray[i] = inputArray[i + 1];
                        inputArray[i + 1] = temp;
                    }
                }
                count++;
            }
            maxStringLength = inputArray[0].length();
            for (int i = 0; i < inputArray.length; i++) {
                if (inputArray[i].length() != maxStringLength) {
                    howManyStringsAreMaximum = i;
                    break;
                }
            }
            String[] outputArray = new String[howManyStringsAreMaximum];
            for (int i = 0; i < outputArray.length; i++) {
                outputArray[i] = inputArray[i];
            }
            return outputArray;
        }
    }
}
