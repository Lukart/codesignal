package throughTheFog;


import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class AbsoluteValuesSumMinimization {
    public static void main(String[] args) {
        int[] a = {1, 2, 2, 4, 7, 100};
        System.out.println(absoluteValuesSumMinimization(a));
    }

    static int absoluteValuesSumMinimization(int[] a) {
        List<Integer> list = new ArrayList<>();
        int[][] b = new int[a.length][a.length];
        int[] sum = new int[a.length];
        for (int i = 0; i < a.length; i++) {
            for (int j = 0; j < a.length; j++) {
                b[i][j] = (Math.abs(a[j] - a[i]));
            }
        }
        for (int i = 0; i < b.length; i++) {
            for (int j = 0; j < b.length; j++) {
                sum[i] += b[i][j];
            }
        }
        int min = Arrays.stream(sum).min().getAsInt();
        for (int i = 0; i < sum.length; i++) {
            if (sum[i] == min) {
                list.add(i);
            }
        }
        int[] c = new int[list.size()];
        for (int i = 0; i <c.length; i++) {
            c[i] = a[list.get(i)];
        }
        int index = 0;
        int minIndex = Arrays.stream(c).min().getAsInt();
        for (int i = 0; i <a.length; i++) {
            if(a[i] == minIndex)
                index = i;
        }
        return a[index];
    }
}
