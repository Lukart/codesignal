package throughTheFog;

public class DepositProfit {
    public static void main(String[] args) {
        int deposit = 100;
        int rate = 1;
        int threshold = 101;
        System.out.println(depositProfit(deposit, rate, threshold));
    }
    static int depositProfit(int deposit, int rate, int threshold) {
        int count = 0;
        while (deposit < threshold){
            deposit += (((double)rate / 100) * deposit);
            count++;
        }
        return count;
    }
}