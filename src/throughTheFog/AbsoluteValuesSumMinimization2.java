package throughTheFog;

public class AbsoluteValuesSumMinimization2 {
    public static void main(String[] args) {
        int[] a = {1, 2, 2, 4, 7, 100};
        System.out.println(absoluteValuesSumMinimization(a));
    }

    static int absoluteValuesSumMinimization(int[] a) {
        return a[(a.length-1)/2];
    }
}
