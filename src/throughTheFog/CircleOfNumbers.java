package throughTheFog;

public class CircleOfNumbers {
    public static void main(String[] args) {
        int n = 10;
        int firstNumber = 7;
        System.out.println(circleOfNumbers(n, firstNumber));
    }
    static int circleOfNumbers(int n, int firstNumber) {
        if(firstNumber < n/2)
        return (n/2) + firstNumber;
        else if(firstNumber == n/2)
            return 0;
        else return (n/2) - (n - firstNumber);
    }
}
