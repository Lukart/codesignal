package edgeOfTheOcean;

import java.util.ArrayList;
import java.util.List;

public class AlmostIncreasingSequence2 {
    public static void main(String[] args) {
        int[] sequence = {1, 2, 3, 4, 99, 5, 6};
        System.out.println(almostIncreasingSequence(sequence));
    }

    static boolean almostIncreasingSequence(int[] sequence) {
        List<Boolean> result = new ArrayList<>();
        int count = 0;
        if (sequence.length <= 2) {
            return true;
        } else
            do {
                List<Integer> list = new ArrayList<>();
                List<Boolean> booleans = new ArrayList<>();
                for (int i = 0; i < sequence.length; i++) {
                    list.add(sequence[i]);
                }
                list.remove(count);
                for (int i = 0; i < list.size() - 1; i++) {
                    if (list.get(i + 1) > list.get(i)) {
                        booleans.add(true);
                    } else
                        booleans.add(false);
                }
                count++;
                if (booleans.contains(false)) {
                } else
                    result.add(true);
            }
            while (count < sequence.length);
        if (result.contains(true))
            return true;
        else
            return false;
    }
}