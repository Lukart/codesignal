package edgeOfTheOcean;

import java.util.Arrays;

public class MakeArrayConsecutive2 {
    public static void main(String[] args) {

        int[] statues = {10,2,3,17,20};
        System.out.println(makeArrayConsecutive2(statues));
    }

    static int makeArrayConsecutive2(int[] statues) {
        Arrays.sort(statues);
        return statues[statues.length - 1] - statues[0] - (statues.length - 2) - 1;
    }
}
