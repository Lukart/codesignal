package edgeOfTheOcean;

public class MatrixElementsSum {
    public static void main(String[] args) {
        int[][] matrix = {  {0, 1, 1, 2},
                            {0, 5, 0, 0},
                            {2, 0, 3, 3}};
        System.out.println(matrixElementsSum(matrix));
    }

    static int matrixElementsSum(int[][] matrix) {
        int result = 0;
        for (int i = 0; i < matrix.length; i++) {
            for (int j = 0; j < matrix[i].length; j++) {
                if (matrix[i][j] == 0) {
                    for (int k = 0; k < matrix.length - i; k++) {
                        matrix[i + k][j] = 0;
                    }
                }else if ( matrix[i][j] != 0) {
                    result += matrix[i][j];

                    }
            }
        }
        return result;
    }
}

