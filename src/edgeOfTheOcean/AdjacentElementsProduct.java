package edgeOfTheOcean;

import java.util.Arrays;

public class AdjacentElementsProduct {

    public static void main(String[] args) {
        int[] inputArray = {1, 3, 6, -2, 3, 4, 4};
        System.out.println(adjacentElementsProduct(inputArray));
    }

    static int adjacentElementsProduct(int[] inputArray) {
        int result;
        int[] sumArray = new int[inputArray.length - 1];
        for (int i = 0; i < inputArray.length - 1; i++) {
            sumArray[i] = (inputArray[i] * inputArray[i+1]);
        }
        Arrays.sort(sumArray);
        result = sumArray[sumArray.length-1];
        return result;
    }
}
