package edgeOfTheOcean;

public class ShapeArea {
    public static void main(String[] args) {

        System.out.println(shapeArea(4));
    }

    static int shapeArea(int n) {
        int poleOfTriangle = 0;
        int sideOfSquare = n + (n - 1);
        for (int i = 0; i < n; i++) {
            poleOfTriangle += i;
        }
        return (sideOfSquare * sideOfSquare) - 4 * (poleOfTriangle);
    }
}
