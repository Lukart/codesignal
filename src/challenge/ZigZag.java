package challenge;

import java.util.ArrayList;
import java.util.List;

public class ZigZag {
    public static void main(String[] args) {
        int[] a = {2, 1, 4, 4, 1, 4, 4, 1, 2, 0, 1, 0, 0, 3, 1, 3, 4, 1, 3, 4};
        System.out.println(zigzag(a));
        //expected result = 6
    }

    static int zigzag(int[] a) {
        List<Integer> list = new ArrayList<>();
        if (a.length <= 2) {
            return 1;
        }
        int counter = 1;
        for (int i = 1; i < a.length - 1; i++) {
            if (a[i] < a[i - 1] && a[i] < a[i + 1] || a[i] > a[i - 1] && a[i] > a[i + 1]) {
                counter++;
                if (i == a.length - 2) {
                    counter++;
                    list.add(counter);
                }
            } else if (counter > 1) {
                counter++;
                list.add(counter);
                counter = 1;
            } else {
                list.add(counter);
                counter = 1;
            }
        }

        return list.stream()
                .mapToInt(i -> i)
                .max().getAsInt();
    }
}
