package challenge;

public class DigitCharactersSum {
    public static void main(String[] args) {
        char ch1 = '3';
        char ch2 = '2';
        System.out.println(digitCharactersSum(ch1, ch2));
    }
    static String digitCharactersSum(char ch1, char ch2) {
        return String.valueOf((int)ch1 - (int)'0' + (int)ch2 - (int)'0');
    }
}
