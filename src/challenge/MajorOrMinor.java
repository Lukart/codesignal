package challenge;

public class MajorOrMinor {

    public static void main(String[] args) {
        String[] scale = {"E1", "F#1", "G1", "A1", "B1", "C2", "D2", "E2"};
        System.out.println(majorOrMinor(scale));
    }

    static String majorOrMinor(String[] scale) {
        String secondKey = scale[1];
        String thirdKey = scale[2];
        String secondKeyWithoutNumber = "";
        String thirdKeyWithoutNumber = "";
        String result = "";
        if (secondKey.length() == 3) {
            secondKeyWithoutNumber = secondKey.substring(0, 2);
        } else if (secondKey.length() == 2) {
            secondKeyWithoutNumber = secondKey.substring(0, 1);
        }
        if (thirdKey.length() == 3) {
            thirdKeyWithoutNumber = thirdKey.substring(0, 2);
        } else if (thirdKey.length() == 2) {
            thirdKeyWithoutNumber = thirdKey.substring(0, 1);
        }
        if (secondKeyWithoutNumber.equals("A") && thirdKeyWithoutNumber.equals("A#") ||
                secondKeyWithoutNumber.equals("A#") && thirdKeyWithoutNumber.equals("B") ||
                secondKeyWithoutNumber.equals("B") && thirdKeyWithoutNumber.equals("C") ||
                secondKeyWithoutNumber.equals("C") && thirdKeyWithoutNumber.equals("C#") ||
                secondKeyWithoutNumber.equals("C#") && thirdKeyWithoutNumber.equals("D") ||
                secondKeyWithoutNumber.equals("D") && thirdKeyWithoutNumber.equals("D#") ||
                secondKeyWithoutNumber.equals("D#") && thirdKeyWithoutNumber.equals("E") ||
                secondKeyWithoutNumber.equals("E") && thirdKeyWithoutNumber.equals("F") ||
                secondKeyWithoutNumber.equals("F") && thirdKeyWithoutNumber.equals("F#") ||
                secondKeyWithoutNumber.equals("F#") && thirdKeyWithoutNumber.equals("G") ||
                secondKeyWithoutNumber.equals("G") && thirdKeyWithoutNumber.equals("G#") ||
                secondKeyWithoutNumber.equals("G#") && thirdKeyWithoutNumber.equals("A"))
            result = "minor";
        else
            result = "major";

        return result;
    }
}

