package challenge;

import java.util.Arrays;

public class FractionSum {
    public static void main(String[] args) {
        int[] a = {3, 5};
        int[] b = {7, 5};
        System.out.println(Arrays.toString(fractionSum(a, b)));
    }
    static int[] fractionSum(int[] a, int[] b) {
        int[] fs = new int[2];
        if(a[1] == b[1]){
            fs[0] = a[0]+b[0];
            fs[1] = a[1];
        }else {
            fs[0] = (a[0] * b[1]) + (b[0] * a[1]);
            fs[1] = a[1] * b[1];
        }
        int num = fs[0];
        int den = fs[1];
        while (num != den){
            if (num > den)
                num -= den;
            else
                den -= num;
        }
        fs[0] = fs[0]/num;
        fs[1] = fs[1]/num;
        return fs;
    }
}