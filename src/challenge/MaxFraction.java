package challenge;
import java.util.Arrays;

public class MaxFraction {
    public static void main(String[] args) {
        int[] numerators = {5, 2, 5};
        int[] denominators = {6, 3, 4};
        System.out.println(maxFraction(numerators, denominators));
    }
    static int maxFraction(int[] numerators, int[] denominators) {
        double[] a = new double[denominators.length];
        for (int i = 0; i <a.length ; i++) {
            a[i] = (double)numerators[i]/denominators[i];
        }
        double max = Arrays.stream(a).max().getAsDouble();
        int result = 0;
        for (int i = 0; i <a.length; i++) {
            if(a[i] == max){
                result = i;
            }
        }
        return result;
    }
}
