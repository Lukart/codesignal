package challenge;

public class SymmetricalMatrix {
    public static void main(String[] args) {
        int n = 5;
        int[][] inputMatrix =  {{0, 0, 0, 1, 0},
                                {1, 0, 0, 0, 0},
                                {0, 0, 0, 0, 0},
                                {0, 0, 0, 1, 0},
                                {0, 0, 0, 0, 0}};
        //expected false
        System.out.println(symmetricalMatrix(n, inputMatrix));
    }

    static boolean symmetricalMatrix(int n, int[][] inputMatrix) {
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) {
                if (inputMatrix[i][j] == inputMatrix[j][i]) {
                    continue;
                }else return false;
            }
        }
        return true;
    }
}
