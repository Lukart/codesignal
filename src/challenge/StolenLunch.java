package challenge;

import java.util.Arrays;

public class StolenLunch {
    public static void main(String[] args) {
        String note = "you'll n4v4r 6u4ss 8t: cdja";
        System.out.println(stolenLunch(note));
    }
    static String stolenLunch(String note) {
        char[] array = note.toCharArray();
        for (int i = 0; i <array.length ; i++) {
            if(note.charAt(i) == 'a') array[i] = '0';
            else if(note.charAt(i) == 'b') array[i] = '1';
            else if(note.charAt(i) == 'c') array[i] = '2';
            else if(note.charAt(i) == 'd') array[i] = '3';
            else if(note.charAt(i) == 'e') array[i] = '4';
            else if(note.charAt(i) == 'f') array[i] = '5';
            else if(note.charAt(i) == 'g') array[i] = '6';
            else if(note.charAt(i) == 'h') array[i] = '7';
            else if(note.charAt(i) == 'i') array[i] = '8';
            else if(note.charAt(i) == 'j') array[i] = '9';
            else if(note.charAt(i) == '0') array[i] = 'a';
            else if(note.charAt(i) == '1') array[i] = 'b';
            else if(note.charAt(i) == '2') array[i] = 'c';
            else if(note.charAt(i) == '3') array[i] = 'd';
            else if(note.charAt(i) == '4') array[i] = 'e';
            else if(note.charAt(i) == '5') array[i] = 'f';
            else if(note.charAt(i) == '6') array[i] = 'g';
            else if(note.charAt(i) == '7') array[i] = 'h';
            else if(note.charAt(i) == '8') array[i] = 'i';
            else if(note.charAt(i) == '9') array[i] = 'j';
        }
        return String.valueOf(array);
    }
}
