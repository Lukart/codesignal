package challenge;

public class MyCoffee {
    public static void main(String[] args) {
        int number = 3;
        System.out.println(myCoffee(number));
    }
    static String myCoffee(int number) {
        if(number == 1)
            return "French Roast";
        else if (number == 2)
            return "Colombian";
        return "Kona";
    }
}
