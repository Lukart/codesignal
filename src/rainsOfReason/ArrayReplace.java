package rainsOfReason;

import java.util.Arrays;

public class ArrayReplace {
    public static void main(String[] args) {
        int elemToReplace = 1;
        int substitutionElem = 3;
        int[] inputArray = {1, 2, 1};
        System.out.println(Arrays.toString(arrayReplace(inputArray, elemToReplace, substitutionElem)));
    }

    static int[] arrayReplace(int[] inputArray, int elemToReplace, int substitutionElem) {
        for (int i = 0; i <inputArray.length; i++) {
            if(inputArray[i] == elemToReplace){
                inputArray[i]= substitutionElem;
            }
        }
        return inputArray;
    }
}
