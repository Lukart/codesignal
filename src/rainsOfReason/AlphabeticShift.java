package rainsOfReason;

import java.util.Arrays;

public class AlphabeticShift {
    public static void main(String[] args) {
        String inputString = "crazy";
        System.out.println(alphabeticShift(inputString));
    }
    static String alphabeticShift(String inputString) {
        char firstLetter = 'a';
        char lastLetter = 'z';
        int shift = 1;
        int range = lastLetter - firstLetter + 1;
        char[] result = new char[inputString.length()];

        for (int i = 0; i < inputString.length(); i++) {
            int newLetterValue = inputString.charAt(i) + shift;
            if (newLetterValue > lastLetter) {
                newLetterValue = newLetterValue - range;
            }
            result[i] = (char) newLetterValue;
        }
        return String.valueOf(result);
    }
}
