package rainsOfReason;

public class EvenDigitsOnly {
    public static void main(String[] args) {
        int n = 248622;
        System.out.println(evenDigitsOnly(n));
    }
    static boolean evenDigitsOnly(int n) {
        String numb = String.valueOf(n);
        for (int i = 0; i < numb.length() ; i++) {
            int temp = Character.getNumericValue(numb.charAt(i));
            if(temp % 2 == 1){
                return false;
            }
        }
        return true;
    }
}
