package exploringtheWaters;

import java.util.Arrays;

public class AlternatingSums {
    public static void main(String[] args) {
        int[] a = {50, 60, 60, 45, 70};
        System.out.println(Arrays.toString(alternatingSums(a)));
        //expected result int[] result = {180, 105}
    }

    static int[] alternatingSums(int[] a) {
        int sum1 = 0;
        int sum2 = 0;
        for (int i = 0; i < a.length ; i++) {
            if(i % 2 == 0){
                sum1 += a[i];
            }else {
                sum2 += a[i];
            }
        }
        int[] sum = {sum1, sum2};
        return sum;
    }
}
