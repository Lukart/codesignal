package exploringtheWaters;

public class ArrayChange {
    public static void main(String[] args) {
        int[] inputArray = {1, -100, 0};
        System.out.println(arrayChange(inputArray));
    }

    static int arrayChange(int[] inputArray) {
        int result = 0;
        for (int i = 0; i < inputArray.length - 1; i++) {
            if (inputArray[i + 1] <= inputArray[i]) {
                int temp = inputArray[i] - inputArray[i+1] + 1;
                inputArray[i + 1] += temp;
                result += temp;
            }
        }
        return result;
    }
}
