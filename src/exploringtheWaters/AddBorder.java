package exploringtheWaters;


import java.util.Arrays;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class AddBorder {
    public static void main(String[] args) {

        String[] picture = {"abc",
                            "ded"};

        System.out.println(Arrays.toString(addBorder(picture)));
    }

    static String[] addBorder(String[] picture) {
        String[] result = new String[picture.length + 2];
        String temp = Stream
                .generate(() -> String.valueOf('*'))
                .limit(picture[0].length()+2)
                .collect(Collectors.joining());
        result[0] = temp;
        result[result.length - 1] = temp;
        for (int i = 0; i < picture.length ; i++) {
            result[i+1] = "*"+picture[i]+"*";
        }
        return result;
    }
}
