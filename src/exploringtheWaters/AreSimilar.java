package exploringtheWaters;


import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class AreSimilar {
    public static void main(String[] args) {

        int[] a = {1, 2, 3};
        int[] b = {1, 2, 3};
        System.out.println(areSimilar(a, b));
    }

    static boolean areSimilar(int[] a, int[] b) {
        List<Integer> check = new ArrayList<>();
        if (a.length != b.length)
            return false;
        else {
            for (int i = 0; i < a.length; i++) {
                if (a[i] == b[i])
                    continue;
                else {
                    check.add(a[i]);
                    check.add(b[i]);
                }
            }
        }
        if (check.size() > 4)
            return false;
        else if (check.size() == 0)
            return true;
        else {
            Collections.sort(check);
            for (int i = 0; i < check.size(); i++) {
                if ((check.get(0).equals(check.get(1))) && (check.get(2).equals(check.get(3))))
                    return true;
                else
                    return false;
            }
        }return false;
    }
}
