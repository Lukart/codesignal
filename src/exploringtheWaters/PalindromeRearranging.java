package exploringtheWaters;

import java.util.BitSet;

public class PalindromeRearranging {
    public static void main(String[] args) {
        String inputString = "aabbaaqccffrrggtteewwxy";
        System.out.println(palindromeRearranging(inputString));
    }

    static boolean palindromeRearranging(String inputString) {
        char[] charArray = inputString.toLowerCase().toCharArray();
        BitSet bitSet = new BitSet();
        for (int i = 0; i < charArray.length; i++)
                bitSet.flip(charArray[i]);
        if (bitSet.stream().count() > 1) {
            return false;
        }return true;
    }
}
