package theJourneyBegins;

public class CenturyFromYear {
    public static void main(String[] args) {

        System.out.println(centuryFromYear(1891));
    }

    static int centuryFromYear(int year) {
        int result = 0;
        if (year >= 1 && year <= 100)
            result = 1;
        else if (year >= 101 && year <= 200)
            result = 2;
        else if (year >= 201 && year <= 300)
            result = 3;
        else if (year >= 301 && year <= 400)
            result = 4;
        else if (year >= 401 && year <= 500)
            result = 5;
        else if (year >= 501 && year <= 600)
            result = 6;
        else if (year >= 601 && year <= 700)
            result = 7;
        else if (year >= 701 && year <= 800)
            result = 8;
        else if (year >= 801 && year <= 900)
            result = 9;
        else if (year >= 901 && year <= 1000)
            result = 10;
        else if (year >= 1001 && year <= 1100)
            result = 11;
        else if (year >= 1101 && year <= 1200)
            result = 12;
        else if (year >= 1201 && year <= 1300)
            result = 13;
        else if (year >= 1301 && year <= 1400)
            result = 14;
        else if (year >= 1401 && year <= 1500)
            result = 15;
        else if (year >= 1501 && year <= 1600)
            result = 16;
        else if (year >= 1601 && year <= 1700)
            result = 17;
        else if (year >= 1701 && year <= 1800)
            result = 18;
        else if (year >= 1801 && year <= 1900)
            result = 19;
        else if (year >= 1901 && year <= 2000)
            result = 20;
        else if (year >= 2001 && year <= 2100)
            result = 21;
        return result;
    }
}
