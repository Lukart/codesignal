package theJourneyBegins;

public class CheckPalindrome {

    public static void main(String[] args) {
        System.out.println(checkPalindrome("a    nn A"));
    }
    static boolean checkPalindrome(String inputString) {
        String inputStringWithoutSpace = inputString.replace(" ", "");
        StringBuilder stringBuilder = new StringBuilder(inputStringWithoutSpace);
        String result = stringBuilder.reverse().toString();
        if (!inputStringWithoutSpace.equalsIgnoreCase(result))
            return false;
        else
        return true;
    }
}
