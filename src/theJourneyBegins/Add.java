package theJourneyBegins;

public class Add {
    public static void main(String[] args) {
        System.out.println(add(3, 7));
    }

    static int add(int param1, int param2) {
        return param1 + param2;
    }
}
