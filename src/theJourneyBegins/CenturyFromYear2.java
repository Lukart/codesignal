package theJourneyBegins;

public class CenturyFromYear2 {
    public static void main(String[] args) {

        System.out.println(centuryFromYear(1900));
    }

    static int centuryFromYear(int year) {
        return ((year-1)/100)+1;
    }
}
