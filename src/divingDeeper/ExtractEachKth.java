package divingDeeper;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class ExtractEachKth {
    public static void main(String[] args) {
        int[] inputArray = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10};
        int k = 3;
        System.out.println(Arrays.toString(extractEachKth(inputArray, k)));
    }

    static int[] extractEachKth(int[] inputArray, int k) {

        int[] arr = new int[10];
        int count = 0;
        int bound = inputArray.length;
        for (int i = 0; i < bound; i++) {
            if (((i + 1) % k) != 0) {
                int i1 = inputArray[i];
                if (arr.length == count) arr = Arrays.copyOf(arr, count * 2);
                arr[count++] = i1;
            }
        }
        arr = Arrays.copyOfRange(arr, 0, count);
        return arr;
    }
}
