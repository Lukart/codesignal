package divingDeeper;

import java.util.*;

public class DifferentSymbolsNaive {
    public static void main(String[] args) {
        String s = "abcdabc";       //expected output int = 4
        System.out.println(differentSymbolsNaive(s));
    }
    static int differentSymbolsNaive(String s) {
        Set<Character> set = new TreeSet<>();
        for (int i = 0; i <s.length() ; i++) {
            set.add(s.charAt(i));
        }
        return set.size();
    }
}
