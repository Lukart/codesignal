package divingDeeper;

public class FirstDigit {
    public static void main(String[] args) {
        String inputString = "a a_933";
        System.out.println(firstDigit(inputString));
    }

    static char firstDigit(String inputString) {
        for (int i = 0; i < inputString.length(); i++) {
            if (inputString.charAt(i) >= 48 && inputString.charAt(i) <= 57) {
                return inputString.charAt(i);
            }
        }
        return '0';
    }
}
