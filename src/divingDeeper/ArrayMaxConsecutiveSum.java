package divingDeeper;

import java.util.Arrays;

public class ArrayMaxConsecutiveSum {
    public static void main(String[] args) {
        int[] inputArray = {2, 3, 5, 1, 6};
        int k = 3;
        System.out.println(arrayMaxConsecutiveSum(inputArray, k));
    }

    static int arrayMaxConsecutiveSum(int[] inputArray, int k) {
        int[] sum = new int[inputArray.length - (k - 1)];
        for (int i = 0; i < inputArray.length - (k - 1); i++) {
            sum[i] = Arrays.stream(inputArray, i, i+k).sum();
        }
        return Arrays.stream(sum).max().getAsInt();
    }
}
