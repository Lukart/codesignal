package darkWilderness;

public class LongestDigitsPrefix {
    public static void main(String[] args) {
        String inputString = "abc123aabc12";

        System.out.println(longestDigitsPrefix(inputString));
    }

    static String longestDigitsPrefix(String inputString) {
        return inputString.replaceAll("^(\\d*).*","$1");
    }
}




//(inputString.charAt(i)>=48&&inputString.charAt(i)<=57){