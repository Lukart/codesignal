package darkWilderness;

public class KnapsackLight {
    public static void main(String[] args) {
        int value1 = 3;
        int weight1 = 5;
        int value2 = 3;
        int weight2 = 8;
        int maxW = 10;
        System.out.println(knapsackLight(value1, weight1, value2, weight2, maxW));
    }

    static int knapsackLight(int value1, int weight1, int value2, int weight2, int maxW) {
        if (maxW < weight1 && maxW < weight2) {
            return 0;
        } else if (weight1 > maxW && weight1 + weight2 > maxW) {
            return value2;
        } else if (weight2 > maxW && weight1 + weight2 > maxW) {
            return value1;
        } else if (weight1 <= maxW && weight1 + weight2 > maxW && value2 >= value1) {
            return value2;
        } else if (weight1 <= maxW && weight1 + weight2 > maxW && value1 > value2) {
            return value1;
        }
        return value1 + value2;
    }
}








