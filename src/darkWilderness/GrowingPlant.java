package darkWilderness;

public class GrowingPlant {
    public static void main(String[] args) {
        int upSpeed = 10;
        int downSpeed = 9;
        int desireHeight = 4;
        System.out.println(growingPlant(upSpeed, downSpeed, desireHeight));
    }

    static int growingPlant(int upSpeed, int downSpeed, int desiredHeight) {
        if(upSpeed >= desiredHeight)
            return 1;
        int count = 0;
        int actualHeight = 0;
        while(actualHeight < desiredHeight - downSpeed) {
            actualHeight = actualHeight + upSpeed - downSpeed;
            count++;
        }
        return count;
    }
}
